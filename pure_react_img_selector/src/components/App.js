import React, { Component } from "react";

import {
  Route,
  NavLink,
  HashRouter,
} from "react-router-dom";

import SelectedImagePage from "./SelectedImagePage";
import SearchPage from "./SearchPage";

import { addImage } from '../actions'

export default class App extends Component {
	
	constructor(props) {
		super(props)
		this.state = {
	        imageList: []
	    }
	}
	
	addToCandidateList = (img) => {
		this.props.store.dispatch( addImage(img) )
	}
	
	removeFromFinalSelection = (imageList) => {
    	this.setState({
    		imageList: imageList
        })
	}
	
	addCandidateToFinalSelection = img => {

		console.log("App::addCandidateToFinalSelection")
				
		if (img === null)
		{
			console.log("addCandidateToFinalSelection img null")
			return;
		}

		if (this.state.imageList.filter(obj => {
			return obj.id === img.id}).length === 0)
		{
	    	this.setState(prevState => ({
	            imageList: [ ...prevState.imageList, img ]
	        }))
		}
		else
		{
			alert("Image already added")
		}
	}
	
	render() {
	  
		const {
			imageList,
			candidateList
		} = this.state;

		return (
		<HashRouter>
	
        <div>
          <h1>Simple Image Selector</h1>
          <ul className="header">
            <li><NavLink  exact to="/">Selection</NavLink></li>
            <li><NavLink  to="/search">Search</NavLink></li>
          </ul>
          <div className="content">	  
				<Route exact path="/" 
					render={(props) =>
						<SelectedImagePage
							imageList={imageList}
							removeFromFinalSelection=
								{this.removeFromFinalSelection}
						/> } />

				<Route path="/search" 
					render={(props) =>
						<SearchPage
							addToCandidateList={this.addToCandidateList}
							addCandidateToFinalSelection=
								{this.addCandidateToFinalSelection}
						/> } />
          </div>
        </div>
	</HashRouter>
    );
  }
}
