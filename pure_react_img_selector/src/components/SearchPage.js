import React, { Component } from "react";
import Select from 'react-select';
import ImageList from './ImageList'
import { CANDIDATE_LIST_MODE } from './Constants';

export default class SearchPage extends Component {
	
	static loading = true;
	static totalImageList = [];
	
	constructor(props) {
		super(props)
		this.state = {
			imgSelectedBySelector: null,
			loadedOnce: true
	    }
	}
	
	addCandidateToFinalSelection = (img, candidateIdToMoveToFinalList) => {
		
		let filtered = this.props.candidateList.filter(
    		img => img.id !== candidateIdToMoveToFinalList);

        this.props.addCandidateToFinalSelection(img, filtered);
	}

	handleChange = (img, candidateList) => {
		
		if (img === null)
		{
			// It happens when the select is cleared
			return false;
		}
		
		if (candidateList.filter(obj => {
				return obj.id === img.id}).length === 0)
		{
			this.props.addToCandidateList(img)
		}
		else
		{
			alert('Already added')
		}
	}
	
	// load dropdown 
	componentDidMount = () => {
		
		console.log("SearchPage componentDidMount loading", SearchPage.loading)
		
		if (!SearchPage.loading)
		{
			return;
		}
		
		fetch('https://jsonplaceholder.typicode.com/photos')
			.then(response => response.json())
			//.then(json => console.log("response", json))
			.then(json => {
				
			    console.log("fetching")
				json.forEach(elem => {
					elem.value = elem.id;
					elem.label = elem.title;
				});

			    SearchPage.loading = false;
			    
			    SearchPage.totalImageList = json;
			    
			    this.setState({
			    	loadedOnce: false
			    })
			});
	}
	
	render() {

		const loading = SearchPage.loading
		const totalImageList = SearchPage.totalImageList

		const {
			candidateList,
			imgSelectedBySelector
		} = this.props

		var self = this;
			
	    return (
			<div>
			{
				(loading) ?
					<div>loading</div> :
					<div>
				    	<Select
				    		className="div-select"
				    		isClearable={true}
				        	value={imgSelectedBySelector}
				        	onChange={(img) => {
				        		self.handleChange(img,candidateList)
				        	}}
				        	options={totalImageList} />
				
				    	<ImageList imageList={candidateList}
							add={this.addCandidateToFinalSelection}
							mode={CANDIDATE_LIST_MODE}
				    	/>
	
		    		</div>
			}
	    	</div>
		);
	}
}
