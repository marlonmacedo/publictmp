import React from 'react'

class ImageDisplay extends React.Component {

	render() {
	
		const {image} = this.props
		
		return (
    
			<div className="image-display">
			
				<img src={image.url} alt={image.title} />
			</div>
    	)
	}
}

export default ImageDisplay
