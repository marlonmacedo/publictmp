import React from 'react'

class Image extends React.Component {

	displayImage = (img) => {
		console.log('display image', img)
		window.open(img.url, null,'width=1000,height=1000,resizable=1');
	}

	render() {
		
		const {
			img,
			mode,
			//
			markId,
			checked,
			// <button>
			onClick,
			btnText,
			displayBtn,
			// <img>
			imgOnClick
		} = this.props
		
		if (!img)
		{
			return (<div>Cannot display image</div>)
		}
		
		let self = this

		return (
			<section className="image-comp">
			<div>{img.title}</div>

			<input type="checkbox" 
				checked={checked}
				name="" value="{id}"
				onClick={(e) => 
					markId(mode, e.target.checked? img.id : null)}
			/>
			
			<div>
			{
				(displayBtn) ?
					<button onClick={() => {onClick(img)}}>
					{btnText}
					</button> :
					<span></span>
			}
			</div>

			<div>
				<img src={img.thumbnailUrl} 
					alt={img.title}
					title={img.title}
					onClick={()=> {
						(imgOnClick) ? self.displayImage(img) : void(0)
					}}
				/>
			</div>
			
			</section>
		)
	}
}

export default Image
