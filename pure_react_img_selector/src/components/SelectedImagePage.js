import React, { Component } from 'react'
import ImageList from './ImageList'
import { FINAL_SELECTION_LIST_MODE } from './Constants';

export default class SelectedImagePage extends Component {

	removeFromFinalSelection = (listAfterRemoval) => {
	    this.props.removeFromFinalSelection(listAfterRemoval);
	}
	
	render() {
		const {
			imageList
		} = this.props
	
		return (
			<div>
				<ImageList 
					imageList={imageList}
					remove={this.removeFromFinalSelection}
					mode={FINAL_SELECTION_LIST_MODE}
				/>
			</div>
		)
	}
}
