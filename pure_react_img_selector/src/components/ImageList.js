import React from 'react'
import Image from './Image'
import { CANDIDATE_LIST_MODE } from './Constants';

class ImageList extends React.Component {

	constructor(props) {
		super(props)
		this.state = {
			idToRemove: null,
	        idToAdd: null
	    }
	}

	remove = (img) => {
		const {idToRemove} = this.state

        let filtered = this.props.imageList.filter(
	        img => img.id !== idToRemove);

		this.setState({ idToRemove: null })
	    
	    this.props.remove(filtered);
	}

	add = (img) => {
		console.log("ImageList::add")
		const {idToAdd} = this.state

		this.props.add(img, idToAdd);
		
    	this.setState({ idToAdd: null })
	}
	
	markId = (mode, imgId) => {
		
		if (CANDIDATE_LIST_MODE === mode) {
			this.setState({
				idToAdd: imgId
			});
		}
		else {
			this.setState({
				idToRemove: imgId
			});
		}
	}
	
	render() {
	
		const {
			idToRemove,
			idToAdd
		} = this.state
		
		//
		const {
			imageList,
			mode
		} = this.props
			
		return (
    
			<div className="image-list">
			
				<p></p>
			
				{(imageList.length === 0) ?
					<p>No images listed</p> :
					imageList.map(image =>
						<Image 
							key={image.id}
							img={image}
							markId={this.markId}
							add={this.add}
							remove={this.remove}
							mode={mode}
							displayBtn={
								(mode === CANDIDATE_LIST_MODE) ?
									(image.id === idToAdd) : 
									(image.id === idToRemove)
							}
							// <btn>
							onClick={
								(mode === CANDIDATE_LIST_MODE) ?
									this.add: this.remove
							}
							btnText={
								(mode === CANDIDATE_LIST_MODE) ?
									"Add to seletion" :
									"Remove"
							}
							checked={
								(mode === CANDIDATE_LIST_MODE) ?
									(image.id === idToAdd) : 
									(image.id === idToRemove)
							}
							// <img>
							imgOnClick={(mode !== CANDIDATE_LIST_MODE)}
						/>
					)
        	}
			</div>
    	)
	}
}

export default ImageList
