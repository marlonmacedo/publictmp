import React from 'react'
import Image from './Image'

class ImageList extends React.Component {

	render() {
	
		const {
			imageList,
			mode,
			btnText,
			onCheck,
			currentChecked,
			remove,
			isImageClickable
		} = this.props
		
		imageList.map(img => console.log('ImageList img.id', img.id))
		
		//console.log('current checked', currentChecked)
		return (
    
			<div className="image-list">
			
				<p></p>
			
				{(imageList.length === 0) ?
					<p>No images listed</p> :
					imageList.map(image =>
						<Image
							key={image.id}
							img={image}
							onCheck={onCheck}
							remove={this.remove}
							mode={mode}
							// <btn>
							onBtnClick={remove}
							btnText={btnText}
							checked={(currentChecked !== null &&
									image.id === currentChecked.id)}
							// <img>
							isImageClickable={isImageClickable}
						/>
					)
        	}
			</div>
    	)
	}
}

export default ImageList
