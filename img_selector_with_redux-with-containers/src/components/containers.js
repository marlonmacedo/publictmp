import React, { Component } from "react";
import PropTypes from 'prop-types'
import SearchPage from './SearchPage'
import { 
	setImageSelectedFromSelector,
	addCandidateImage,
	removeCandidateImage,
	checkCandidateImage
} from '../actions'

export const SearchPageContainer = (props, { store }) => {

	const {
		imageSelectedFromSelector,
		candidateList,
		checkedCandidateImg
	} = store.getState()

    return (<SearchPage
    	candidateList = {candidateList}
    	checkedCandidateImg = {checkedCandidateImg}
    	imageSelectedFromSelector = {imageSelectedFromSelector}
    	setImageSelectedFromSelector = {img => 
    		store.dispatch( setImageSelectedFromSelector(img) )
    	}
    	addCandidateImage = {img => 
    		store.dispatch( addCandidateImage(img) )		
    	}
    	removeCandidateImage = {id => 
    		store.dispatch( removeCandidateImage(id) )		
    	}
    	checkCandidateImage = {img => 
    		store.dispatch( checkCandidateImage(img) )	
    	}
		afterRemoval = {props.afterRemoval}
	/>)
}

SearchPageContainer.contextTypes = {
    store: PropTypes.object
}
