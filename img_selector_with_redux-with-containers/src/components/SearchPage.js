import React, { Component } from "react";
import Select from 'react-select';
import ImageList from './ImageList'
import PropTypes from 'prop-types'


// Presentational Component
// Except for the loading status and the list of images that
// can be selected
class SearchPage extends Component {
	
	static loading = true;
	static totalImageList = [];
	
	constructor(props, context) {
		super(props)
		
		this.state = {
			loadedOnce: true
	    }
	}
	
	remove = img => {
		
		this.props.removeCandidateImage(img.id)
		this.props.checkCandidateImage(null)
		
		console.log('SearchPage remove img', img)
        this.props.afterRemoval(img);
	}

	// preset for removal
	check = img => {
		console.log('SearchPage check img', img)
		this.props.checkCandidateImage(img)
	}
	
	handleChange = img => {
		console.log('SearchPage handleChange', img)

		this.props.setImageSelectedFromSelector(img)
		
		if (img === null)
		{
			// It happens when the select is cleared
			return false;
		}
		
		if (this.props.candidateList.filter(obj => {
			return obj.id === img.id}).length === 0)
		{
			this.props.addCandidateImage(img)
		}
		else
		{
			alert('Already added')
		}
	}
	
	// load dropdown 
	componentDidMount = () => {
		
		console.log("SearchPage componentDidMount loading", SearchPage.loading)
		
		if (!SearchPage.loading)
		{
			return;
		}
		
		fetch('https://jsonplaceholder.typicode.com/photos')
			.then(response => response.json())
			//.then(json => console.log("response", json))
			.then(json => {
				
			    console.log("fetching")
				json.forEach(elem => {
					elem.value = elem.id;
					elem.label = elem.title;
				});

			    SearchPage.loading = false;
			    
			    SearchPage.totalImageList = json;
			    
			    this.setState({
			    	loadedOnce: false
			    })
			});
	}
	
	render() {

		const loading = SearchPage.loading
		const totalImageList = SearchPage.totalImageList

		// Retrieve from store
		const { 
	    	candidateList,
	    	checkedCandidateImg,
			imageSelectedFromSelector
		} = this.props
		
		console.log('candidateList', candidateList)
		
		console.log('SearchPage render imgSelectedFromSelector',
				imageSelectedFromSelector)
		console.log('SearchPage checked img', checkedCandidateImg)

		var self = this;
			
	    return (
			<div>
			{
				(loading) ?
					<div>loading</div> :
					<div>
				    	<Select
				    		className="div-select"
				    		isClearable={true}
				        	value={imageSelectedFromSelector}
				        	onChange={(img) => {
				        		self.handleChange(img)
				        	}}
				        	options={totalImageList} />
				
				    	<ImageList
				    		imageList={candidateList}
							remove={this.remove}
				    		onCheck={this.check}
				    		currentChecked={checkedCandidateImg}
				    		btnText="Add to selection"
				    		isImageClickable={false}
				    	/>
	
		    		</div>
			}
	    	</div>
		);
	}
}

SearchPage.contextTypes = {
	store: PropTypes.object
}

export default SearchPage
