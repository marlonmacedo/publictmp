import C from './constants'

export const setImageSelectedFromSelector = (img) =>
    ({
        type: C.SET_IMG_SELECTED_FROM_SELECTOR,
        imageSelectedFromSelector: img
    })
    
export const addCandidateImage = img =>
    ({
    	id: img.id,
    	type: C.ADD_CANDIDATE_IMAGE,
        title: img.title,
        url: img.url,
        thumbnailUrl: img.thumbnailUrl,
        albumId: img.albumId,
        value: img.value,
        label: img.label
    })

export const removeCandidateImage = id =>
    ({
        type: C.REMOVE_CANDIDATE_IMAGE,
        id
    })

export const addFinalImage = img =>
    ({
    	id: img.id,
    	type: C.ADD_FINAL_IMAGE,
        title: img.title,
        url: img.url,
        thumbnailUrl: img.thumbnailUrl,
        albumId: img.albumId,
        value: img.value,
        label: img.label
    })

export const removeFinalImage = id =>
    ({
        type: C.REMOVE_FINAL_IMAGE,
        id
    })
    
export const checkCandidateImage = (img) =>
    ({
        type: C.CHECK_CANDIDATE_IMAGE,
        checkedCandidateImg: img
    })    
    
export const checkFinalImage = (img) =>
    ({
        type: C.CHECK_FINAL_IMAGE,
        checkedFinalImg: img
    })    