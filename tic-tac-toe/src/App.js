import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import logo from './logo.svg';
import './App.css';


////////////////////////////////////////////////////////////////////////////////
// You can use the keyboard, to navigate over the board use  tab, shift-tab

window.addEventListener('mousedown', function(e) {
  document.body.classList.add('mouse-navigation');
  document.body.classList.remove('kbd-navigation');
});
window.addEventListener('keydown', function(e) {
  if (e.keyCode === 9) {
    document.body.classList.add('kbd-navigation');
    document.body.classList.remove('mouse-navigation');
  }
});
window.addEventListener('click', function(e) {
  if (e.target.tagName === 'A' && e.target.getAttribute('href') === '#') {
    e.preventDefault();
  }
});
window.onerror = function(message, source, line, col, error) {
	let errors;
  var text = error ? error.stack || error : message + ' (at ' + source + ':' + line + ':' + col + ')';
  errors.textContent += text + '\n';
  errors.style.display = '';
};
console.error = (function(old) {
  return function error() {
	let errors;
    errors.textContent += Array.prototype.slice.call(arguments).join(' ') + '\n';
    errors.style.display = '';
    old.apply(this, arguments);
  }
})(console.error);

class Square extends React.Component {

	render() {
  		let boxClass = ["square"]
        boxClass = boxClass.concat( this.props.winnerClass );

  		return (
			<div className={boxClass.join(' ')} onClick={this.props.onClick}>
			{this.props.value}
			</div>
  		);
	}
}


////////////////////////////////////////////////////////////////////////////////

class Board extends React.Component {
  renderSquare(i) {
    //console.log("Board::renderSquare()");
    return (
      <Square
        winnerClass={this.props.winnerClassList[i]}
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  render() {
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array(9).fill(null)
        }
      ],
      stepNumber: 0,
      xIsNext: true
    };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
	
    console.log("handleClick.i " + i);
	console.log("handleClick.history " + JSON.stringify(history));

    const current = history[history.length - 1]; // the last one

    // slice() no arguments: copy the entire array

    // the current one is the last one in history plus the modification
    const currentSquares = current.squares.slice();

    console.log("currentSquares " + JSON.stringify(currentSquares));

    // stop if winner found or do nothing is clicked on already checked box

    var winnerResult = calculateWinner(currentSquares);

    if (winnerResult) {

	
      return;
    }

    if ( currentSquares[i] ) {
		return;
	}

    // and here you have the modification
    currentSquares[i] = this.state.xIsNext ? "X" : "O";
    this.setState({
      
      // the outer  square brackets can be misleading
      // See:   https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_concat
      // Think of concat as a kind of merge
      history: history.concat([
        {
          squares: currentSquares
        }
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext
    });
  }

  jumpTo(step) {
    console.log("jumpTo step " + step);
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0
    });
  }

  render() {
    console.log("Game::render()");
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winnerResult = calculateWinner(current.squares);
    let classList = Array(9).fill();

    let winner = null
    if (winnerResult)
    {
        let winnerSymbol = winnerResult[0];
		let boxes = winnerResult[1];
		let type = winnerResult[2];
 
      	console.log(  "winner2 " + JSON.stringify(boxes) + " type: " + type );

		for (var winnerIndex in boxes) {
            console.log("winnerIndex " + boxes[winnerIndex]);
  			classList[boxes[winnerIndex]] = [type, "symbol" + winnerSymbol];
		}

	  	console.log(  "classList " + JSON.stringify(classList) );

       winner = winnerResult[0];
	}

	console.log("Game::render winnerClassList " + JSON.stringify( this.state.winnerClassList ));

    const moves = history.map((step, move) => {
      const desc = move ?
        'Go to move #' + move :
        'Go to game start';
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    let status;
    if (winner) {
      status = "Winner: " + winner;
    } else {
      status = "Next player: " + (this.state.xIsNext ? "X" : "O");
    }


    // moves is a list of <li></li>
    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
			winnerClassList = {classList}
            onClick={i => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>   
        </div>
      </div>
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

ReactDOM.render(<Game />, document.getElementById("root"));

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];

    let type = 'horiz';   //horiz, vertical, bottomUp, topBottom

    // check  squares[a] have a symbol and others have the same symbol
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {

			if ((a == 0 && b == 3 && c == 6) ||
				(a == 1 && b == 4 && c == 7) ||
				(a == 2 && b == 5 && c == 8)  )
			{  
				type = 'vertical';
			}
			else if (a == 0 && b == 4 && c == 8)
			{
				type = 'topBottom';
			}
			else if (a == 2 && b == 4 && c == 6)
			{
				type = 'bottomUp';
			}

			return [squares[a], lines[i], type];
		}
	}
	return null;
}


export default Game;
