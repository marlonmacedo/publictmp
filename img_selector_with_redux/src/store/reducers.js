import C from '../constants'

export const imageSelectedFromSelector =
	(state = {}, action) => {
    switch (action.type) {
    case C.SET_IMG_SELECTED_FROM_SELECTOR:
        return action.imageSelectedFromSelector
    default :
        return state
    }
}

export const checkedCandidateImg =
	(state = {}, action) => {
    switch (action.type) {
    case C.CHECK_CANDIDATE_IMAGE:
        return action.checkedCandidateImg
    default :
        return state
    }
}

export const checkedFinalImg =
	(state = {}, action) => {
    switch (action.type) {
    case C.CHECK_FINAL_IMAGE:
        return action.checkedFinalImg
    default :
        return state
    }
}

export const image = (state = {}, action) => {
    switch (action.type) {
        case C.ADD_CANDIDATE_IMAGE:
            return {
                id: action.id,
                title: action.title,
                url: action.url,
                thumbnailUrl: action.thumbnailUrl,
                albumId: action.albumId,
                value: action.value,
                label: action.label
            }
        case C.ADD_FINAL_IMAGE:
            return {
                id: action.id,
                title: action.title,
                url: action.url,
                thumbnailUrl: action.thumbnailUrl,
                albumId: action.albumId,
                value: action.value,
                label: action.label
            }

        default :
            return state
    }
}

export const candidateList = (state = [], action) => {
    switch (action.type) {
    case C.ADD_CANDIDATE_IMAGE :
        return [
            ...state,
            image({}, action)
        ]
    case C.REMOVE_CANDIDATE_IMAGE :
        return state.filter(
            img => img.id !== action.id
        )
    default:
        return state
    }
}

export const finalList = (state = [], action) => {
    switch (action.type) {
    case C.ADD_FINAL_IMAGE :
        return [
            ...state,
            image({}, action)
        ]
    case C.REMOVE_FINAL_IMAGE :
        return state.filter(
            img => img.id !== action.id
        )
    default:
        return state
    }
}