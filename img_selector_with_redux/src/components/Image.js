import React from 'react'

class Image extends React.Component {

	displayImage = (img) => {
		console.log('display image', img)
		window.open(img.url, null,'width=1000,height=1000,resizable=1');
	}

	render() {
		
		const {
			img,
			//
			checked,
			onCheck,
			// <button>
			onBtnClick,
			btnText,
			// <img>
			isImageClickable
		} = this.props
		
		//console.log("image ", img.id, checked)

		if (!img)
		{
			return (<div>Cannot display image</div>)
		}
		
		let self = this

		return (
			<section className="image-comp">

			<input type="checkbox"
				checked={checked}
				name=""
				onChange={(e) => 
					onCheck(e.target.checked? img : null)}
			/>

			<div>
			{
				(checked) ?
					<button onClick={() => {onBtnClick(img)}}>
					{btnText}
					</button> :
					<span></span>
			}
			</div>

			<div>{img.title}</div>
			
			<div>
				<img src={img.thumbnailUrl} 
					alt={img.title}
					title={img.title}
					onClick={()=> {
						(isImageClickable) ? self.displayImage(img) : void(0)
					}}
				/>
			</div>
			
			</section>
		)
	}
}

export default Image
