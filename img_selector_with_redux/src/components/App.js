import React, { Component } from "react";

import {
  Route,
  NavLink,
  HashRouter,
} from "react-router-dom";

import PropTypes from 'prop-types'

import SelectedImagePage from "./SelectedImagePage";
import SearchPage from "./SearchPage";

import {
	addFinalImage
} from '../actions'

class App extends Component {
	
	getChildContext() {
		return {
			store: this.props.store
		}
	}

    componentWillMount() {
        this.unsubscribe = this.props.store.subscribe(
            () => this.forceUpdate()
        )
    }

    componentWillUnmount() {
        this.unsubscribe()
    }
    
	addCandidateToFinalSelection = img => {

		console.log("App::addCandidateToFinalSelection")
				
		if (img === null)
		{
			console.log("addCandidateToFinalSelection img null")
			return;
		}

		const { finalList } = this.props.store.getState()
		 
		if (finalList.filter(obj => {
			return obj.id === img.id}).length === 0)
		{
			this.props.store.dispatch( addFinalImage(img) )
		}
		else
		{
			alert("Image already added")
		}
	}
	
	render() {
	  
		return (
		<HashRouter>
	
        <div>
          <h1>Simple Image Selector</h1>
          <ul className="header">
            <li><NavLink  exact to="/">Selection</NavLink></li>
            <li><NavLink  to="/search">Search</NavLink></li>
          </ul>
          <div className="content">	  
				<Route exact path="/" 
					render={(props) =>
						<SelectedImagePage />
					} />

				<Route path="/search" 
					render={(props) =>
						<SearchPage
							afterRemoval=
								{this.addCandidateToFinalSelection}
						/>
						} />
          </div>
        </div>
	</HashRouter>
    );
  }
}
	
App.propTypes = {
	store: PropTypes.object.isRequired
}

App.childContextTypes = {
	store: PropTypes.object.isRequired
}

export default App
