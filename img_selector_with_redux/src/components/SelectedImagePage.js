import React, { Component } from 'react'
import ImageList from './ImageList'

import PropTypes from 'prop-types'

import {
	checkFinalImage,
	removeFinalImage
} from '../actions'

export default class SelectedImagePage extends Component {

	remove = img => {
		const {store} = this.context
		
		store.dispatch( removeFinalImage(img.id) )
		store.dispatch( checkFinalImage(null) )
	}
	
	// preset for removal
	check = img => {
		console.log('SelectedImagePage check img', img)
		const {store} = this.context
		store.dispatch( checkFinalImage(img) )
	}

	render() {
		
		const {store} = this.context
		
		const {
			finalList,
			checkedFinalImg
		} = store.getState()
	
		return (
			<div>
				<ImageList 
					imageList={finalList}
					remove={this.remove}
					onCheck={this.check}
					currentChecked={checkedFinalImg}
	    			btnText="Remove"
	    			isImageClickable={true}
				/>
			</div>
		)
	}
}

SelectedImagePage.contextTypes = {
	store: PropTypes.object
}
